// ============================================================================
// Allan CORNET - 2011
// ============================================================================
#include "stdafx.h"
#include <objbase.h>  
#include "ComScilabServer_h.h"
#include "ComScilabServer_i.c"
#include <atlbase.h> // CComBSTR
#include "Registry.h"
#include "ComScilabServerImpl.h"
// ============================================================================
static long g_cComponents = 0 ;     // Count of active components
static long g_cServerLocks = 0 ;    // Count of locks
// ============================================================================
static HMODULE g_hModule = NULL ;   // DLL module handle
// Friendly name of component
const char g_szFriendlyName[] = "COMServer for Scilab" ;

// Version-independent ProgID
const char g_szVerIndProgID[] = "Scilab.Application" ;

// ProgID
const char g_szProgID[] = "Scilab.Application.1" ;
// ============================================================================
//
// Constructor
//
CoCOMServer::CoCOMServer() : m_cRef(1)
{ 
    InterlockedIncrement(&g_cComponents) ; 

    m_ptinfo = NULL;
    LoadTypeInfo(&m_ptinfo, LIBID_SLApp, IID_IScilabApp, 0);
}
// ============================================================================
//
// Destructor
//
CoCOMServer::~CoCOMServer() 
{ 
    InterlockedDecrement(&g_cComponents) ; 
}
// ============================================================================
//
// IUnknown implementation
//
HRESULT STDMETHODCALLTYPE CoCOMServer::QueryInterface(const IID& iid, void** ppv)
{    
    if (iid == IID_IUnknown || iid == IID_IScilabApp || iid == IID_IDispatch)
    {
        *ppv = static_cast<IScilabApp*>(this) ; 
    }
    else
    {
        *ppv = NULL ;
        return E_NOINTERFACE ;
    }
    reinterpret_cast<IUnknown*>(*ppv)->AddRef() ;
    return S_OK ;
}
// ============================================================================
ULONG STDMETHODCALLTYPE CoCOMServer::AddRef()
{
    return InterlockedIncrement(&m_cRef) ;
}
// ============================================================================
ULONG STDMETHODCALLTYPE CoCOMServer::Release() 
{
    if (InterlockedDecrement(&m_cRef) == 0)
    {
        delete this ;
        ::PostMessage(NULL,WM_QUIT,0,0);
        return 0 ;
    }
    return m_cRef ;
}
// ============================================================================
// IScilabApp implementation
// ============================================================================
int STDMETHODCALLTYPE  CoCOMServer::putCommand(/*in*/BSTR objectname)
{
    return putCommandInScilabQueue(objectname);
}
// ============================================================================
int CoCOMServer::Quit(void)
{
    return putCommandInScilabQueue(L"quit");
}
// ============================================================================
int STDMETHODCALLTYPE CoCOMServer::Execute(BSTR commandLine)
{
	int iErr = executeCommand(commandLine);
	return iErr;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::getScalarDouble( 
    /* [in] */ BSTR varname,
    /* [retval][out] */ double *pValue)
{
	if (getNamedScalarDouble(varname, pValue)) return S_OK;
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::getScalarString( 
	/* [in] */ BSTR varname,
	/* [out] */ BSTR *strval)
{
    wchar_t *pwstr = NULL;
    if (getAllocatedNamedSingleWideString(varname, &pwstr))
    {
        SysReAllocString(strval, pwstr);
        freeAllocatedSingleWideString(pwstr);
        pwstr = NULL;
        return S_OK;
    }
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::getScalarBoolean( 
    /* [in] */ BSTR varname,
    /* [retval][out] */ VARIANT_BOOL *pValue)
{
    bool bValue = false;
    if (::getNamedScalarBoolean(varname, &bValue))
    {
        *pValue = bValue == true ? VARIANT_TRUE : VARIANT_FALSE;
        return S_OK;
    }
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::createScalarDouble( 
    /* [in] */ BSTR varname,
    /* [in] */ double pValue)
{
    if (::createNamedScalarDouble(varname, pValue))
    {
        return S_OK;
    }
    return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::createScalarString( 
    /* [in] */ BSTR varname,
    /* [in] */ BSTR strval)
{
    if (::createNamedSingleWideString(varname, strval))
    {
        return S_OK;
    }

    return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::createScalarBoolean( 
    /* [in] */ BSTR varname,
    /* [in] */ VARIANT_BOOL bValue)
{
	if (::createNamedScalarBoolean(varname, (bValue == VARIANT_TRUE) ? true : false))
	{
		return S_OK;
	}

    return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::getComplexMatrixOfDouble( 
	/* [in] */ BSTR varname,
	/* [out][in] */ SAFEARRAY * *pr,
	/* [out][in] */ SAFEARRAY * *pi)
{
	int rows = 0;
	int cols = 0;
	double *pdReal = NULL;
	double *pdImg = NULL;

	if (getNamedComplexMatrixOfDouble(varname, &rows, &cols, pdReal, pdImg))
	{
		pdReal = new double[rows * cols];
		pdImg = new double[rows * cols];
		if ((pdReal == NULL) || (pdImg == NULL))
		{
			return S_FALSE;
		}
	}
	else
	{
		return S_FALSE;
	}

	if (getNamedComplexMatrixOfDouble(varname, &rows, &cols, pdReal, pdImg))
	{
		if ((SafeArrayDestroy(*pr) == S_OK) && (SafeArrayDestroy(*pi) == S_OK))
		{
			SAFEARRAYBOUND sab[2];

			sab[0].lLbound = 1;
			sab[0].cElements = rows;
			sab[1].lLbound = 1;
			sab[1].cElements = cols;

			*pr = SafeArrayCreate(VT_R8, 2, sab);
			*pi = SafeArrayCreate(VT_R8, 2, sab);

			for(int j = 1; j <= cols; j++) 
			{
				for(int i = 1; i <= rows; i++) 
				{
					// Create entry value for (i,j)
					double pdValueReal = pdReal[(i-1) +(j-1) * rows];
					double pdValueImg = pdImg[(i-1) +(j-1) * rows];
					
					// Add to safearray...
					long indices[] = {i,j};
					if (SafeArrayPutElement(*pr, indices, (void *)&pdValueReal) != S_OK)
					{
						return S_FALSE;
					}

					if (SafeArrayPutElement(*pi, indices, (void *)&pdValueImg) != S_OK)
					{
						return S_FALSE;
					}
				}
			}
			return S_OK;
		}
	}
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::getMatrixOfDouble( 
	/* [in] */ BSTR varname,
	/* [out][in] */ SAFEARRAY * *pr)
{
	int rows = 0;
	int cols = 0;
	double *pdReal = NULL;
	
	if (getNamedMatrixOfDouble(varname, &rows, &cols, pdReal))
	{
		pdReal = new double[rows * cols];
		if (pdReal == NULL)
		{
			return S_FALSE;
		}
	}
	else
	{
		return S_FALSE;
	}

	if (getNamedMatrixOfDouble(varname, &rows, &cols, pdReal))
	{
		if (SafeArrayDestroy(*pr) == S_OK) 
		{
			SAFEARRAYBOUND sab[2];

			sab[0].lLbound = 1;
			sab[0].cElements = rows;
			sab[1].lLbound = 1;
			sab[1].cElements = cols;

			*pr = SafeArrayCreate(VT_R8, 2, sab);

			for(int j = 1; j <= cols; j++) 
			{
				for(int i = 1; i <= rows; i++) 
				{
					// Create entry value for (i,j)
					double pdValueReal = pdReal[(i-1) +(j-1) * rows];

					// Add to safearray...
					long indices[] = {i,j};
					if (SafeArrayPutElement(*pr, indices, (void *)&pdValueReal) != S_OK)
					{
						return S_FALSE;
					}
				}
			}
			return S_OK;
		}
	}
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::createMatrixOfDouble( 
	/* [in] */ BSTR varname,
	/* [in] */ SAFEARRAY * *pr)
{
	int rows = 0, cols = 0;
	
	if (pr)
	{
		if (GetDimensions(*pr, &rows, &cols))
		{
			bool bOK = false;
			double *dValues = NULL;
			
			if (cols == 0)
			{
				dValues = new double[rows];
			}
			else
			{
				dValues = new double[rows * cols];
			}
			
			if (dValues)
			{
				int k = 0;
				
				if (cols == 0)
				{
					for (int j = 0; j < rows; j++)
					{
						double dVal = 0.;
						void *dvoid = &dVal;
						long lDimension[2];

						lDimension[0] = j;
						lDimension[1] = 0;
						HRESULT hRes = SafeArrayGetElement(*pr, lDimension, dvoid);
						if (hRes != S_OK)
						{
							delete [] dValues;
							dValues = NULL;
							return S_FALSE;
						}
						dValues[k] = dVal;
						k++;
					}
				}
				else
				{
					for (int i = 0 ; i < cols; i++)
					{
						for (int j = 0; j < rows; j++)
						{
							double dVal = 0.;
							void *dvoid = &dVal;
							long lDimension[2];

							lDimension[0] = j;
							lDimension[1] = i;
							HRESULT hRes = SafeArrayGetElement(*pr, lDimension, dvoid);
							if (hRes != S_OK)
							{
								delete [] dValues;
								dValues = NULL;
								return S_FALSE;
							}
							dValues[k] = dVal;
							k++;
						}
					}
				}

				if (cols == 0)
				{
					bOK = createNamedMatrixOfDouble(varname, rows, 1, dValues);
				}
				else
				{
					bOK = createNamedMatrixOfDouble(varname, rows, cols, dValues);
				}

				delete [] dValues;
				dValues = NULL;

				if (bOK)
				{
					return S_OK;
				}
			}
		}
	}
	return S_FALSE;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::isNamedVarExist( 
	/* [in] */ BSTR varname,
	/* [retval][out] */ VARIANT_BOOL *isExist)
{
	*isExist = VARIANT_FALSE;
	if (::isNamedVarExist(varname) == true)
	{
		*isExist = VARIANT_TRUE;
	}
	return S_OK;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::get_Visible(VARIANT_BOOL* pValue)
{
    *pValue = isVisibleMainWindow() == true ? VARIANT_TRUE : VARIANT_FALSE;
    return S_OK;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::put_Visible(VARIANT_BOOL newValue)
{
    setVisibleMainWindow(newValue == VARIANT_TRUE ? true : false);
    return S_OK;
}
// ============================================================================
HRESULT CoCOMServer::LoadTypeInfo(ITypeInfo ** pptinfo, const CLSID &libid, const CLSID &iid, LCID lcid)
{
    HRESULT hr;
    LPTYPELIB ptlib = NULL;
    LPTYPEINFO ptinfo = NULL;

    *pptinfo = NULL;

    // Load type library.
    hr = LoadRegTypeLib(libid, 1, 0, lcid, &ptlib);
    if (FAILED(hr))
        return hr;

    // Get type information for interface of the object.
    hr = ptlib->GetTypeInfoOfGuid(iid, &ptinfo);
    if (FAILED(hr))
    {
        ptlib->Release();
        return hr;
    }

    ptlib->Release();
    *pptinfo = ptinfo;
    return NOERROR;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::GetTypeInfoCount(UINT* pctinfo)
{
    *pctinfo = 1;
    return S_OK;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::GetTypeInfo(UINT itinfo, LCID lcid, ITypeInfo** pptinfo)
{
    *pptinfo = NULL;

    if(itinfo != 0)
        return ResultFromScode(DISP_E_BADINDEX);

    m_ptinfo->AddRef();      // AddRef and return pointer to cached
    // typeinfo for this object.
    *pptinfo = m_ptinfo;

    return NOERROR;
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames,
    LCID lcid, DISPID* rgdispid)
{
    return DispGetIDsOfNames(m_ptinfo, rgszNames, cNames, rgdispid);
}
// ============================================================================
HRESULT STDMETHODCALLTYPE CoCOMServer::Invoke(DISPID dispidMember, REFIID riid,
    LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult,
    EXCEPINFO* pexcepinfo, UINT* puArgErr)
{
    return DispInvoke(
        this, m_ptinfo,
        dispidMember, wFlags, pdispparams,
        pvarResult, pexcepinfo, puArgErr); 
}
// ============================================================================
//
// Class factory IUnknown implementation
//
HRESULT STDMETHODCALLTYPE CFactory::QueryInterface(const IID& iid, void** ppv)
{    
    if ((iid == IID_IUnknown) || (iid == IID_IClassFactory))
    {
        *ppv = static_cast<IClassFactory*>(this) ; 
    }
    else
    {
        *ppv = NULL ;
        return E_NOINTERFACE ;
    }
    reinterpret_cast<IUnknown*>(*ppv)->AddRef() ;
    return S_OK ;
}
// ============================================================================
ULONG STDMETHODCALLTYPE CFactory::AddRef()
{
    return InterlockedIncrement(&m_cRef) ;
}
// ============================================================================
ULONG STDMETHODCALLTYPE CFactory::Release() 
{
    if (InterlockedDecrement(&m_cRef) == 0)
    {
        delete this ;
        return 0 ;
    }
    return m_cRef ;
}
// ============================================================================
//
// IClassFactory implementation
//
HRESULT STDMETHODCALLTYPE CFactory::CreateInstance(IUnknown* pUnknownOuter,
    const IID& iid,
    void** ppv) 
{
    // Cannot aggregate.
    if (pUnknownOuter != NULL)
    {
        return CLASS_E_NOAGGREGATION ;
    }

    // Create component.
    CoCOMServer* pA = new CoCOMServer ;
    if (pA == NULL)
    {
        return E_OUTOFMEMORY ;
    }

    // Get the requested interface.
    HRESULT hr = pA->QueryInterface(iid, ppv) ;

    // Release the IUnknown pointer.
    // (If QueryInterface failed, component will delete itself.)
    pA->Release() ;
    return hr ;
}
// ============================================================================
// LockServer
HRESULT STDMETHODCALLTYPE CFactory::LockServer(BOOL bLock) 
{
    if (bLock)
    {
        InterlockedIncrement(&g_cServerLocks) ; 
    }
    else
    {
        InterlockedDecrement(&g_cServerLocks) ;
    }
    return S_OK ;
}
// ============================================================================
///////////////////////////////////////////////////////////
//
// Exported functions
//
// ============================================================================
//
// Can DLL unload now?
//
STDAPI DllCanUnloadNow()
{
    if ((g_cComponents == 0) && (g_cServerLocks == 0))
    {
        return S_OK ;
    }
    else
    {
        return S_FALSE ;
    }
}
// ============================================================================
//
// Get class factory
//
STDAPI DllGetClassObject(const CLSID& clsid,
    const IID& iid,
    void** ppv)
{
    // Can we create this component?
    if (clsid != CLSID_CoCOMServer)
    {
        return CLASS_E_CLASSNOTAVAILABLE ;
    }

    // Create class factory.
    CFactory* pFactory = new CFactory ;  // Reference count set to 1
    // in constructor
    if (pFactory == NULL)
    {
        return E_OUTOFMEMORY ;
    }

    // Get requested interface.
    HRESULT hr = pFactory->QueryInterface(iid, ppv) ;
    pFactory->Release() ;

    return hr ;
}
// ============================================================================
CFactory gClassFactory;
// ============================================================================
DWORD CoEXEInitialize()
{
    DWORD nReturn;

    HRESULT hr=::CoRegisterClassObject(CLSID_CoCOMServer,
        &gClassFactory,
        CLSCTX_SERVER, 
        REGCLS_SINGLEUSE, 
        &nReturn);

    return nReturn;
}
// ============================================================================
void CoEXEUninitialize(DWORD nToken)
{
    ::CoRevokeClassObject(nToken);
}
// ============================================================================
//
// Server registration
//
STDAPI DllRegisterServer()
{

    g_hModule = ::GetModuleHandle(NULL);

    HRESULT hr= RegisterServer(g_hModule, 
        CLSID_CoCOMServer,
        g_szFriendlyName,
        g_szVerIndProgID,
        g_szProgID,
        LIBID_SLApp) ;
    if (SUCCEEDED(hr))
    {
        RegisterTypeLib( g_hModule, NULL);
    }
    return hr;
}
// ============================================================================
//
// Server unregistration
//
STDAPI DllUnregisterServer()
{

    g_hModule = ::GetModuleHandle(NULL);

    HRESULT hr= UnregisterServer(CLSID_CoCOMServer,
        g_szVerIndProgID,
        g_szProgID,
        LIBID_SLApp) ;
    if (SUCCEEDED(hr))
    {
        UnRegisterTypeLib( g_hModule, NULL);
    }
    return hr;
}
// ============================================================================
//
// DLL module information
//
BOOL APIENTRY DllMain(HANDLE hModule,
    DWORD dwReason,
    void* lpReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        g_hModule = (HMODULE)hModule ;
    }

    return TRUE ;
}
// ============================================================================
