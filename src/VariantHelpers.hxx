// ============================================================================
// Allan CORNET - 2011
// ============================================================================
#ifndef __VARIANTHELPERS_HXX__
#define __VARIANTHELPERS_HXX__

#include "stdAfx.h"


typedef enum { 
	ExcelErrDiv0  = -2146826281,
	ExcelErrNA    = -2146826246,
	ExcelErrName  = -2146826259,
	ExcelErrNull  = -2146826288,
	ExcelErrNum   = -2146826252,
	ExcelErrRef   = -2146826265,
	ExcelErrValue = -2146826273
} ExcelErrEnum;

bool GetDimensions(SAFEARRAY* pSafeArray, int *m, int *n);

#endif /* __VARIANTHELPERS_HXX__ */
// ============================================================================

