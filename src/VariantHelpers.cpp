// ============================================================================
// Allan CORNET - 2011
// ============================================================================
#include "stdAfx.h"
// ============================================================================
#define DIM_MAX 2
// ============================================================================
bool GetDimensions(SAFEARRAY* pSafeArray, int *m, int *n)
{
	UINT i = 0;
	UINT DimSafeArray = 0;
	UINT dimensions[DIM_MAX];

	DimSafeArray = SafeArrayGetDim(pSafeArray);

	/* We manage only array with 2 dimensions */
	if (DimSafeArray == 1)
	{
		long lUpperBound = 0;
		long lLowerBound = 0;

		if (FAILED (SafeArrayGetLBound (pSafeArray, 1, &lLowerBound)))
		{
			// error
			*m = -1;
			*n = -1;
			return false;
		}
		if (FAILED (SafeArrayGetUBound (pSafeArray, 1, &lUpperBound)))
		{
			// error
			*m = -1;
			*n = -1;
			return false;
		}
		
		*m = lUpperBound - lLowerBound + 1;
		*n = 0; 

		return true;
	}
	else if (DimSafeArray == DIM_MAX) 
	{
		for (i = 0;i < DimSafeArray;i++)
		{
			long lUpperBound = 0;
			long lLowerBound = 0;

			if (FAILED (SafeArrayGetLBound (pSafeArray,i+1,&lLowerBound)))
			{
				// error
				*m = -1;
				*n = -1;
				return false;
			}
			if (FAILED (SafeArrayGetUBound (pSafeArray,i+1,&lUpperBound)))
			{
				// error
				*m = -1;
				*n = -1;
				return false;
			}
			dimensions[i] = lUpperBound - lLowerBound + 1;
		}

		*m = dimensions[0];
		*n = dimensions[1];
		return true;
	}
	return false;
}
// ============================================================================
