// ============================================================================
// Allan CORNET - 2011
// ============================================================================
#ifndef __SCILABWRAP_HXX__
#define __SCILABWRAP_HXX__

bool loadSymbols(void);
bool freeSymbols(bool bFreeDlls);
bool isSymbolsLoaded(void);
bool loadScilabDlls(void);
bool freeScilabDlls(void);

int Windows_Main(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nCmdShow);

void setVisibleMainWindow(bool newVisibleState);
bool isVisibleMainWindow(void);

bool putCommandInScilabQueue(wchar_t *wcommand);
int executeCommand(wchar_t *wcommand);

bool createNamedMatrixOfDouble(const wchar_t* _pstName, int _iRows, int _iCols, const double* _pdblReal);
bool getNamedMatrixOfDouble(const wchar_t* _pstName, int* _piRows, int* _piCols, double* _pdblReal);

bool getNamedScalarDouble(const wchar_t* _pstName, double* _pdblReal);
bool createNamedScalarDouble(const wchar_t* _pstName, double _dblReal);

bool createNamedSingleWideString(const wchar_t* _pstName, const wchar_t* _pwstStrings);
bool getAllocatedNamedSingleWideString(const wchar_t* _pstName,  wchar_t** _pwstData);
void freeAllocatedSingleWideString(wchar_t* _pwstData);

bool getNamedScalarBoolean(const wchar_t* _pstName, bool* _pbool);
bool createNamedScalarBoolean(const wchar_t* _pstName, bool _bool);

bool getNamedComplexMatrixOfDouble(const wchar_t* _pstName, int* _piRows, int* _piCols, double* _pdblReal, double* _pdblImg);


int getLastErrorValue(void);

int getNamedVarType(const wchar_t* _pstName);
bool isNamedVarComplex(const wchar_t *_pstName);
bool isNamedVarExist(const wchar_t* _pstName);
bool isNamedScalar(const wchar_t* _pstName);
bool isNamedEmptyMatrix(const wchar_t* _pstName);

#endif /* __SCILABWRAP_HXX__ */
// ============================================================================