//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================
#ifndef __FILEEXISTS_H__
#define __FILEEXISTS_H__

#include <string>

bool fileExists(std::wstring filename);

#endif /* __FILEEXISTS_H__ */
//=============================================================================
